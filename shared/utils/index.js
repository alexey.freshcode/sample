const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const utils = {};

const files = fs.readdirSync(__dirname);
files.forEach(fileName => {

	const fullPath = path.join(__dirname, fileName);
	if ( fs.statSync(fullPath).isDirectory() ) {
		return utils[ fileName ] = require(fullPath);
	}

	const baseName = path.basename(fileName, '.js');
	const extension = path.extname(fileName);

	if ( extension !== '.js' || baseName === 'index' ) {
		return;
	}
	utils[ _.camelCase(baseName) ] = require(fullPath);
});

module.exports = utils;
