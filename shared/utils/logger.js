const fs = require('fs');
const path = require('path');
require('winston-daily-rotate-file');
const { Syslog } = require('winston-syslog');
const { createLogger, format, transports, config } = require('winston');
const { combine, timestamp, colorize, printf } = format;

const logDir = './logs/';

// create logs directory if necessary
if ( !fs.existsSync(logDir) ) {
	fs.mkdirSync(logDir);
}

const fileTransport = new transports.DailyRotateFile({
	filename: path.join(logDir, 'log'),
	datePattern: 'dd-MM-yyyy.',
	prepend: true,
	level: process.env.DEBUG_LEVEL || 'error'
});

const myFormat = printf(info => {
	return `${info.timestamp} ${info.level}: ${info.message}`;
});

const logger = createLogger({
	transports: [
		new transports.Console({
			format: combine(
				timestamp(),
				colorize(),
				myFormat
			),
			level: process.env.DEBUG_LEVEL || 'info',
			stderrLevels: [ 'error', 'emerg', 'crit' ],
			prettyPrint: true
		}),
		new Syslog({
			app_name: 'sample'
		}),
		fileTransport
	]
});

logger.setLevels(config.syslog.levels);

module.exports = logger;
