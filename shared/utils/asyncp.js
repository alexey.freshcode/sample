const _ = require('lodash');
const util = require('util');
const async = require('async');

module.exports = _.reduce([
	'each',
	'eachOf',
	'eachLimit',
	'eachSeries',
	'eachOfSeries',
	'eachOfLimit',
	'retry',
	'map',
	'mapSeries',
	'mapValues',
	'mapValuesSeries',
	'reduce',
	'transform',
	'whilst',
	'parallel',
	'timesSeries',
], (result, value) => {
	result[ value ] = util.promisify(async[ value ]);
	return result;
}, {});