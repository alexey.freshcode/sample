const _ = require('lodash');

module.exports = {
	trim(instance) {
		_.forOwn(instance.dataValues, (value, key) => {
			if ( _.isString(value) ) {
				instance[ key ] = _.trim(value);
			}
		});
	},
};
