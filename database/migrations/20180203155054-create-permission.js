module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Permissions', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER
			},
			order: {
				type: Sequelize.INTEGER,
				defaultValue: 0
			},
			name: {
				type: Sequelize.STRING,
				allowNull: false,
				unique: true
			},
			key: {
				type: Sequelize.STRING,
				allowNull: false,
				unique: true
			},
			createdAt: {
				allowNull: false,
				type: Sequelize.DATE
			},
			updatedAt: {
				allowNull: false,
				type: Sequelize.DATE
			}
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Permissions');
	}
};