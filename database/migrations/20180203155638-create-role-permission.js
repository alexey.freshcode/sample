module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('RolePermissions', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER
			},
			access: {
				type: Sequelize.ENUM,
				values: [ 'read', 'write', 'none' ],
				defaultValue: 'none'
			},
			roleId: {
				type: Sequelize.INTEGER,
				references: {
					model: 'Roles',
					key: 'id'
				},
				onDelete: 'cascade',
				onUpdate: 'cascade'
			},
			permissionId: {
				type: Sequelize.INTEGER,
				references: {
					model: 'Permissions',
					key: 'id'
				},
				onDelete: 'cascade',
				onUpdate: 'cascade'
			}
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('RolePermissions');
	}
};