module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Sessions', {
			sid: {
				allowNull: false,
				primaryKey: true,
				unique: true,
				type: 'varchar(32)'
			},
			expires: {
				type: Sequelize.DATE,
			},
			data: {
				type: Sequelize.TEXT,
			},
			createdAt: {
				allowNull: false,
				type: Sequelize.DATE
			},
			updatedAt: {
				allowNull: false,
				type: Sequelize.DATE
			}
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Sessions');
	}
};