module.exports = {
	development: {
		username: process.env.DB_USERNAME,
		password: process.env.DB_PASSWORD,
		database: process.env.DB_NAME,
		host: process.env.DB_HOST || 'localhost',
		port: process.env.DB_PORT || 5432,
		dialect: 'postgres',
		dialectOptions: {
			ssl: false
		},
		seederStorage: 'sequelize',
		define: {
			timestamps: true
		},
		freezeTableName: true,
		pool: {
			max: 20,
			min: 0,
			idle: 10000
		},
		logging: console.log
	},
	production: {
		username: process.env.DB_USERNAME,
		password: process.env.DB_PASSWORD,
		database: process.env.DB_NAME,
		host: process.env.DB_HOST || 'localhost',
		port: process.env.DB_PORT || 5432,
		dialect: 'postgres',
		dialectOptions: {
			ssl: false
		},
		seederStorage: 'sequelize',
		define: {
			timestamps: true
		},
		freezeTableName: true,
		pool: {
			max: 20,
			min: 0,
			idle: 10000
		},
		logging: false
	},
};
