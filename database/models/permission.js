const utils = require('../utils');

module.exports = (sequelize, DataTypes) => {
	const Permission = sequelize.define('Permission', {
		order: DataTypes.INTEGER,
		name: DataTypes.STRING,
		key: DataTypes.STRING
	}, {
		hooks: {
			beforeCreate(instance, options) {
				utils.trim(instance);
			},
			beforeUpdate(instance, options) {
				utils.trim(instance);
			}
		}
	});
	Permission.associate = (models) => {
		Permission.belongsToMany(models.Role, {
			through: models.RolePermission,
			as: 'roles',
			foreignKey: 'permissionId',
			otherKey: 'roleId',
			hooks: true
		});
	};
	return Permission;
};