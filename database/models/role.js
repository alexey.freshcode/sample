const utils = require('../utils');

module.exports = (sequelize, DataTypes) => {
	const Role = sequelize.define('Role', {
		name: {
			type: DataTypes.STRING,
			validate: {
				notEmpty: true
			}
		}
	}, {
		hooks: {
			beforeCreate(instance, options) {
				utils.trim(instance);
			},
			beforeUpdate(instance, options) {
				utils.trim(instance);
			}
		}
	});

	Role.associate = (models) => {
		Role.belongsToMany(models.User, {
			through: models.UserRole,
			as: 'users',
			foreignKey: 'roleId',
			otherKey: 'userId',
			hooks: true
		});
		Role.belongsToMany(models.Permission, {
			through: models.RolePermission,
			as: 'permissions',
			foreignKey: 'roleId',
			otherKey: 'permissionId',
			hooks: true
		});
	};

	return Role;
};