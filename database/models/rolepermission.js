module.exports = (sequelize, DataTypes) => {
	return sequelize.define('RolePermission', {
		access: {
			type: DataTypes.ENUM,
			values: [ 'read', 'write', 'none' ],
			defaultValue: 'none'
		},
		roleId: DataTypes.INTEGER,
		permissionId: DataTypes.INTEGER
	}, {
		timestamps: false,
	});
};