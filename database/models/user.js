const utils = require('../utils');
const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = (sequelize, DataTypes) => {
	const User = sequelize.define('User', {
		firstName: {
			type: DataTypes.STRING,
			validate: {
				notEmpty: true
			}
		},
		lastName: {
			type: DataTypes.STRING,
			validate: {
				notEmpty: true
			}
		},
		email: {
			type: DataTypes.STRING,
			validate: {
				isEmail: true,
				notEmpty: true
			}
		},
		password: {
			type: DataTypes.STRING,
			validate: {
				notEmpty: true
			}
		}
	}, {
		getterMethods: {
			name: function () {
				return `${this.getDataValue('firstName')} ${this.getDataValue('lastName')}`;
			}
		},
		hooks: {
			beforeCreate: async (user, options) => {
				utils.trim(user);
				user.password = await bcrypt.hash(user.password, saltRounds);
			},
			beforeUpdate: async (user, options) => {
				utils.trim(user);
				if ( user.changed('password') ) {
					user.password = await bcrypt.hash(user.password, saltRounds);
				}
			}
		}
	});

	User.associate = (models) => {
		User.belongsToMany(models.Role, {
			through: models.UserRole,
			as: 'roles',
			foreignKey: 'userId',
			otherKey: 'roleId',
			hooks: true
		});
	};

	User.prototype.authenticate = function (password) {
		return bcrypt.compare(password, this.password);
	};

	return User;
};