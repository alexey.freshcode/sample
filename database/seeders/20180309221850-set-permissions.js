const _ = require('lodash');

const permissions = [
	{ order: 1, name: 'User management' },
	{ order: 2, name: 'Role management' },
];

module.exports = {
	up: async (queryInterface, Sequelize) => {
		const date = new Date();
		return queryInterface.bulkInsert(
			'Permissions',
			_.map(permissions, perm =>
				_.assign(perm, {
					key: _.kebabCase(perm.name),
					createdAt: date,
					updatedAt: date
				})
			)
		);
	},

	down: async (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('Permissions', null, {});
	}
};
