module.exports = {
	up: async (queryInterface, Sequelize) => {
		const models = require('../models');
		const adminRole = await models.Role.findOne({ where: { name: 'Admin' } });
		await adminRole.createUser({
			firstName: 'Admin',
			lastName: 'Admin',
			email: 'admin@example.com',
			password: 'admin'
		});
	},

	down: async (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('Users', null, {});
	}
};
