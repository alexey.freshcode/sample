module.exports = {
	up: async (queryInterface, Sequelize) => {
		const models = require('../models');
		const { asyncp } = require('../../shared/utils');

		const permissions = await models.Permission.findAll();

		let transaction;
		try {
			transaction = await queryInterface.sequelize.transaction();

			const adminRole = await models.Role.create({ name: 'Admin', }, { transaction });

			await asyncp.each(permissions, async perm => adminRole.addPermission(perm, {
				through: { access: 'write' },
				transaction
			}));

			const userRole = await models.Role.create({
				name: 'User',
			}, { transaction });

			await asyncp.each(permissions, async perm => userRole.addPermission(perm, {
				through: { access: 'read' },
				transaction
			}));

			await transaction.commit();
		} catch ( ex ) {
			if ( transaction ) {
				await transaction.rollback();
			}
			throw ex;
		}
	},

	down: async (queryInterface, Sequelize) => {
		return queryInterface.bulkDelete('Roles', null, {});
	}
};
