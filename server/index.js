const cluster = require('cluster');
const path = require('path');
const os = require('os');
const logger = require('../shared/utils/logger');

const nodeEnv = process.env.NODE_ENV;
const numCPUs = nodeEnv === 'production' ? os.cpus().length : 1;  // start only 1 instance for development mode

logger.debug(`Master ${process.pid} is running`);

// TODO: configure for https when available ssl certs
cluster.setupMaster({
	exec: path.join(__dirname, './worker.js'),
	// args: ['--use', 'http']
	// silent: true
});

// Fork workers.
for ( let i = 0; i < numCPUs; i++ ) {
	cluster.fork();
}

// processes messages from workers
cluster.on('message', (worker, data) => {
	switch ( data.type ) {
		case 'shutdown': {
			cluster.disconnect(() => {
				process.exit(data.code);
			});
		}
			break;
		default: {
			logger.debug('Master process receive: ', data);
		}
	}
});

cluster.on('disconnect', worker => {
	logger.debug('Worker %d disconnected', worker.id);
});
cluster.on('exit', worker => {
	// Replaces the dead worker
	if ( worker.exitedAfterDisconnect ) {
		logger.debug('Worker %d exited', worker.id);
	} else {
		logger.debug('Worker %d died :( and new one re-spawned', worker.id);
		cluster.fork();
	}
});

logger.info('--------------------------');
logger.info('===> 😊  Starting WEB Server . . .');
logger.info('===>  Environment: ' + nodeEnv);
