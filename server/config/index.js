module.exports = {
	sessionSecret: process.env.SESSION_SECRET || 'session-secret',
	basicAuth: {
		username: process.env.BASIC_AUTH_USERNAME || 'sample',
		password: process.env.BASIC_AUTH_PASSWORD || 'sample'
	},
	aws: {
		accessKeyId: 'access-key-id',
		secretAccessKey: 'secret-access-key',
		region: 'aws-s3-region',
		s3: 's3-version',
		buckets: {
			'sample': process.env.AWS_BUCKET || 'bucket-name'
		}
	}
};
