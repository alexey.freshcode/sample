/* Initializing passport.js */
const models = require('../../database/models');
const local = require('./passport/local');

module.exports = (app, passport) => {
	// Configure Passport authenticated session persistence.
	//
	// In order to restore authentication state across HTTP requests, Passport needs
	// to serialize users into and deserialize users out of the session.  The
	// typical implementation of this is as simple as supplying the user ID when
	// serializing, and querying the user record by ID from the database when
	// deserializing.
	passport.serializeUser(async (user, done) => {
		done(null, user.id);
	});

	passport.deserializeUser(async (id, done) => {
		try {
			let user = await models.User.findOne({
				where: { id },
				attributes: { exclude: [ 'password' ] },
				include: [
					{
						model: models.Role,
						as: 'roles',
						include: [
							{
								model: models.Permission,
								as: 'permissions',
								required: false
							}
						],
						required: false
					}
				]
			});

			if ( user ) {
				user = user.toJSON();
			}

			done(null, user);
		} catch ( ex ) {
			done(ex);
		}
	});

	// use the following strategies
	passport.use(local);
};
