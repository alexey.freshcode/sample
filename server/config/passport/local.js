/*
 Configuring local strategy to authenticate strategies
 Code modified from : https://github.com/madhums/node-express-mongoose-demo/blob/master/config/passport/local.js
 */
const { Strategy: LocalStrategy } = require('passport-local');
const models = require('../../../database/models');
const _ = require('lodash');
/*
 By default, LocalStrategy expects to find credentials in parameters named username and password.
 If your site prefers to name these fields differently, options are available to change the defaults.
 */
module.exports = new LocalStrategy({
	usernameField: 'email'
}, async (email, password, done) => {
	try {
		const user = await models.User.findOne({
			where: { email: email },
			include: [
				{
					model: models.Role,
					as: 'roles',
					include: [
						{
							model: models.Permission,
							as: 'permissions',
							required: false
						}
					],
					required: false
				}
			]
		});

		if ( !user ) {
			return done(null, false, { message: 'Incorrect email.' });
		}

		const isMatch = await user.authenticate(password);

		if ( !isMatch ) {
			return done(null, false, { message: 'Incorrect password.' });
		}

		return done(null, _.omit(user.toJSON(), [ 'password' ]));
	} catch ( ex ) {
		done(ex);
	}
});
