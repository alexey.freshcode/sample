const _ = require('lodash');
const models = require('../../database/models');

async function findRole(roleId) {
	return models.Role.findOne({
		where: { id: roleId },
		include: [
			{
				model: models.Permission,
				as: 'permissions',
				required: false
			}
		],
	});
}

module.exports = {
	async create(req, res, next) {
		let transaction;
		try {
			const roleData = req.body;
			const { permissions: newPermissions = [] } = roleData;

			transaction = await models.sequelize.transaction();
			let role = await models.Role.create(roleData, { transaction });
			const permissions = await models.Permission.findAll({ transaction });

			await role.setPermissions(permissions.map(permission => {
				const rolePermission = _.find(newPermissions, newPermission => newPermission.id === permission.id);
				if ( rolePermission ) {
					permission.RolePermission = { access: rolePermission.access };
				}
			}));
			await transaction.commit();
			role = await findRole(role.id)

			res.json({ response: { type: 'success', message: 'Created' }, role });
		} catch ( ex ) {
			if ( transaction ) {
				await transaction.rollback();
			}
			next(ex);
		}
	},

	async find(req, res, next) {
		try {
			const { limit, offset } = req.query;

			//language=SQL
			const select = `
				SELECT
				  "Roles".*,
				  COALESCE(jsonb_agg("Permissions")
				             FILTER (WHERE "Permissions"."id" NOTNULL), '[]') AS permissions,
				  count(*) over()
				FROM "Roles"
				  LEFT JOIN
				  (SELECT
				     "Permissions".*,
				     "RolePermissions"."roleId",
				     "RolePermissions"."access"
				   FROM "Permissions"
				     INNER JOIN "RolePermissions" ON "RolePermissions"."permissionId" = "Permissions"."id") AS "Permissions"
				    ON "Permissions"."roleId" = "Roles"."id"
				GROUP BY "Roles"."id"
			`;

			const roles = await models.sequelize.query(`
				${select}
				${!_.isUndefined(limit) ? `limit $limit` : ''}
				${!_.isUndefined(offset) ? `offset $offset` : ''}
			`, {
				bind: {
					limit: !_.isUndefined(limit) ? +limit : undefined,
					offset: !_.isUndefined(offset) ? +offset : undefined,
				},
				type: models.Sequelize.QueryTypes.SELECT
			});

			res.json({ totalCount: _.chain(roles).first().get('count', 0).toNumber().value(), roles: roles });
		} catch ( ex ) {
			next(ex);
		}
	},

	async findById(req, res, next) {
		try {
			const { roleId } = req.params;

			const role = await findRole(roleId)
			res.json({ role });
		} catch ( ex ) {
			next(ex);
		}
	},

	async update(req, res, next) {
		let transaction;
		try {
			const { roleId } = req.params;
			const roleData = req.body;
			const { permissions: newPermissions = [] } = roleData;

			transaction = await models.sequelize.transaction();
			let [ , [ role ] ] = await models.Role.update(roleData, {
				where: { id: roleId },
				returning: true,
				transaction
			});

			if ( !_.isEmpty(newPermissions) ) {
				const permissions = models.permission.findAll({ transaction });
				await role.setPermissions(permissions.map(permission => {
					const rolePermission = newPermissions.find(newPermission => newPermission.id === permission.id);
					if ( rolePermission ) {
						permission.RolePermission = { access: rolePermission.access };
					}
				}));
			}
			await transaction.commit();

			role = await findRole(roleId)

			res.json({ response: { type: 'success', message: 'Updated' }, role });
		} catch ( ex ) {
			if ( transaction ) {
				await transaction.rollback();
			}
			next(ex);
		}
	},

	async delete(req, res, next) {
		try {
			const { roleId } = req.params;
			await models.Role.destroy({ where: { id: roleId } });
			res.json({ response: { type: 'success', message: 'Deleted' }, });
		} catch ( ex ) {
			next(ex);
		}
	},

	async fetchPermissions(req, res, next) {
		try {
			const permissions = await models.Permission.findAll({
				order: [ [ 'order', 'ASC' ] ]
			});
			res.json({ permissions });
		} catch ( ex ) {
			next(ex);
		}
	},
};
