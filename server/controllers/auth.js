const _ = require('lodash');
const util = require('util');
const logger = require('../../shared/utils/logger');

module.exports = {
	async login(req, res, next) {
		try {
			await util.promisify(req.logIn).call(req, req.user);
			res.json({ user: req.user });
		} catch ( ex ) {
			logger.error(ex);
			next(ex);
		}
	},

	async logout(req, res, next) {
		try {
			req.logOut();
			res.end();
		} catch ( ex ) {
			next(ex);
		}
	},

	async ensureAuthenticated(req, res, next) {
		try {
			if ( !req.isAuthenticated() ) {
				return res.status(401).json({ response: { type: 'error', message: 'Not Authorized.' } });
			}
			return next();
		} catch ( ex ) {
			next(ex);
		}
	},

	/**
	 * Checks if user has required permission for required request
	 * @param {string} permissionRequired
	 * @param {string} accessRequired
	 * @returns {function(req, res, next)}
	 */
	hasPermission(permissionRequired, accessRequired) {
		return async (req, res, next) => {
			try {
				if ( _.some(req.user.roles, role =>
					_.some(role.permissions, permission => permission.key === permissionRequired
						&& _.includes([ accessRequired, 'write' ], permission.RolePermission.access))
				) ) {
					return next();
				}
				return res.status(403).json({ response: { type: 'error', message: 'Permission denied.' } });
			} catch ( ex ) {
				next(ex);
			}
		};
	}
};
