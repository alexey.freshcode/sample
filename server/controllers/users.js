const _ = require('lodash');
const models = require('../../database/models');

async function findUser(userId) {
	return models.User.findOne({
		where: { id: userId },
		attributes: { exclude: [ 'password' ] },
		include: [
			{
				model: models.Role,
				as: 'roles',
				include: [
					{
						model: models.Permission,
						as: 'permissions',
						required: false,
					}
				],
				required: false,
			}
		]
	});
}

module.exports = {
	async create(req, res, next) {
		let transaction;
		try {
			const userData = req.body;
			const { roles = [] } = userData;

			transaction = await models.sequelize.transaction();

			let user = await models.User.create(userData, { transaction });
			await user.setRoles(roles.map(role => role.id), { transaction });
			await transaction.commit();
			// fetches new user with roles and permissions populated
			user = await findUser(user.id);

			res.json({ response: { type: 'success', message: 'Created' }, user });
		} catch ( ex ) {
			if ( transaction ) {
				await transaction.rollback();
			}
			next(ex);
		}
	},

	async find(req, res, next) {
		try {
			const { limit, offset } = req.query;

			//language=SQL
			const select = `
				SELECT
				  "Users".*,
				  trim(concat("Users"."firstName", ' ', "Users"."lastName")) AS name,
				  null																											 as password,
				  coalesce(jsonb_agg("Roles")
				             FILTER (WHERE "Roles"."id" NOTNULL), '[]')      AS roles,
				  count(*)
				  OVER ()
				FROM "Users"
				  LEFT JOIN "UserRoles" ON "Users"."id" = "UserRoles"."userId"
				  LEFT JOIN
				  (
				    SELECT
				      "Roles".*,
				      coalesce(jsonb_agg("Permissions")
				                 FILTER (WHERE "Permissions"."id" NOTNULL), '[]') AS permissions
				    FROM "Roles"
				      LEFT JOIN
				      (
				        SELECT
				          "Permissions".*,
				          "RolePermissions"."roleId",
				          "RolePermissions"."access"
				        FROM "Permissions"
				          INNER JOIN "RolePermissions" ON "RolePermissions"."permissionId" = "Permissions"."id"
				      ) AS "Permissions" ON "Permissions"."roleId" = "Roles"."id"
				    GROUP BY "Roles"."id"
				  ) AS "Roles" ON "Roles"."id" = "UserRoles"."roleId"
				GROUP BY "Users"."id";
			`;

			const users = await models.sequelize.query(`
				${select}
				${!_.isUndefined(limit) ? `limit $limit` : ''}
				${!_.isUndefined(offset) ? `offset $offset` : ''}
			`, {
				bind: {
					limit: !_.isUndefined(limit) ? +limit : undefined,
					offset: !_.isUndefined(offset) ? +offset : undefined,
				},
				type: models.Sequelize.QueryTypes.SELECT
			});

			res.json({
				users: users,
				totalCount: _.chain(users).first().get('count', 0).toNumber().value(),
			});
		} catch ( ex ) {
			next(ex);
		}
	},

	async findById(req, res, next) {
		try {
			const { userId } = req.params;

			const user = await findUser(userId);

			res.json({ user });
		} catch ( ex ) {
			next(ex);
		}
	},

	async update(req, res, next) {
		let transaction;
		try {
			const { userId } = req.params;
			const userData = req.body;
			const { roles = [] } = userData;
			transaction = await models.sequelize.transaction();
			let [ , [ user ] ] = await models.User.update(userData, {
				where: { id: userId },
				returning: true,
				individualHooks: true,
				transaction
			});
			if ( !_.isEmpty(roles) ) {
				await user.setRoles(roles.map(role => role.id), { transaction });
			}
			await transaction.commit();
			// fetches updated user with roles and permissions populated
			user = await findUser(userId);

			res.json({ response: { type: 'success', message: 'Updated' }, user });
		} catch ( ex ) {
			next(ex);
		}
	},

	async delete(req, res, next) {
		try {
			const { userId } = req.params;
			if ( req.user.id === userId ) {
				return res.status(400).json({ response: { type: 'error', message: 'You can not delete yourself' } });
			}
			await models.User.destroy({ where: { id: userId } });

			res.json({ response: { type: 'success', message: 'Deleted' }, });
		} catch ( ex ) {
			next(ex);
		}
	}
};
