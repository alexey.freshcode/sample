const express = require('express');
const rolesCtrl = require('../controllers/roles');
const authCtrl = require('../controllers/auth');

const router = express.Router();

// CRUD
router.route('/')
	/**
	 * @swagger
	 * /api/roles/:
	 *   get:
	 *     tags:
	 *      - roles
	 *     summary: Find roles
	 *     produces:
	 *       - application/json
	 *     parameters:
	 *       - in: query
	 *         required: true
	 *         name: limit
	 *         type: integer
	 *       - in: query
	 *         required: true
	 *         name: offset
	 *         type: integer
	 *     responses:
	 *       200:
	 *          type: object
	 *          properties:
	 *            totalCount:
	 *              type: integer
	 *            roles:
	 *              type: array
	 *              items:
	 *                $ref: '#/definitions/FullRole'
	 */
	.get(rolesCtrl.find)

	/**
	 * @swagger
	 * /api/roles:
	 *   post:
	 *     tags:
	 *      - roles
	 *     summary: Create role
	 *     produces:
	 *       - application/json
	 *     parameters:
	 *       - in: body
	 *         name: body
	 *         required: true
	 *         schema:
	 *          $ref: '#/definitions/Role'
	 *     responses:
	 *       200:
	 *         description: Role has been created
	 *         type: object
	 *         properties:
	 *          role:
	 *            $ref: '#/definitions/FullRole'
	 */
	.post(authCtrl.hasPermission('role-management', 'write'), rolesCtrl.create);

/**
 * @swagger
 * /api/roles/permissions:
 *   get:
 *     tags:
 *      - roles
 *     summary: Fetch all permissions
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         required: true
 *         schema:
 *          $ref: '#/definitions/Permission'
 *     responses:
 *       200:
 *          type: object
 *          properties:
 *            permissions:
 *              type: array
 *              items:
 *                $ref: '#/definitions/FullPermission'
 */
router.get('/permissions', rolesCtrl.fetchPermissions);

router.route('/:roleId')
	/**
	 * @swagger
	 * /api/roles/{:roleId}:
	 *   get:
	 *     tags:
	 *      - roles
	 *     summary: Find role by ID
	 *     produces:
	 *       - application/json
	 *     parameters:
	 *       - in: path
	 *         required: true
	 *         name: roleId
	 *         type: integer
	 *     responses:
	 *       200:
	 *         type: object
	 *         properties:
	 *          role:
	 *            $ref: '#/definitions/FullRole'
	 */
	.get(rolesCtrl.findById)

	/**
	 * @swagger
	 * /api/roles/{:roleId}:
	 *   put:
	 *     tags:
	 *      - roles
	 *     summary: Update role by ID
	 *     produces:
	 *       - application/json
	 *     parameters:
	 *       - in: path
	 *         required: true
	 *         name: roleId
	 *         type: integer
	 *       - in: body
	 *         name: body
	 *         required: true
	 *         schema:
	 *          $ref: '#/definitions/Role'
	 *     responses:
	 *       200:
	 *         description: Role has been updated
	 *         type: object
	 *         properties:
	 *          role:
	 *            $ref: '#/definitions/FullRole'
	 */
	.put(authCtrl.hasPermission('role-management', 'write'), rolesCtrl.update)

	/**
	 * @swagger
	 * /api/roles/{:roleId}:
	 *   delete:
	 *     tags:
	 *      - roles
	 *     summary: Delete role by ID
	 *     parameters:
	 *       - in: path
	 *         required: true
	 *         name: roleId
	 *         type: integer
	 *     responses:
	 *       200:
	 *         description: Role has been deleted
	 */
	.delete(authCtrl.hasPermission('role-management', 'write'), rolesCtrl.delete);

module.exports = router;
