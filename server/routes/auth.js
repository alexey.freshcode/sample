const express = require('express');
const authCtrl = require('../controllers/auth');
const passport = require('passport');
const router = express.Router();

/**
 * @swagger
 * /api/auth/login:
 *   post:
 *     tags:
 *      - auth
 *     summary: Login to the application
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: body
 *         required: true
 *         schema:
 *          type: object
 *          required:
 *            - email
 *            - password
 *          properties:
 *            email:
 *              type: string
 *              example: example@example.com
 *            password:
 *              type: string
 *              example: qwerty
 *     responses:
 *       200:
 *         description: login
 *         type: object
 *         properties:
 *          user:
 *            $ref: '#/definitions/FullUser'
 */
router.post('/login', passport.authenticate('local'), authCtrl.login);

/**
 * @swagger
 * /api/auth/logout:
 *   get:
 *     tags:
 *      - auth
 *     summary: Logout from the application
 *     responses:
 *       200:
 *         description: logout
 */
router.get('/logout', authCtrl.logout);

module.exports = router;
