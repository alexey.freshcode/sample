const express = require('express');
const AWS = require('aws-sdk');
const config = require('../config/');
const router = express.Router();

/**
 * @swagger
 * /storage/files/{:key}:
 *   get:
 *     tags:
 *      - storage
 *     produces:
 *      - image/png
 *      - image/gif
 *      - image/jpeg
 *      - application/xml
 *     summary: Get file from S3
 *     parameters:
 *       - in: path
 *         name: key
 *         required: true
 *     responses:
 *       200:
 *         description: The route doesn't configured by default
 *         schema:
 *          type: file
 */
router.get('/files/:key', async (req, res, next) => {
	try {
		const s3 = new AWS.S3();
		const { key } = req.params;

		return s3.getObject({
			Bucket: config.aws.buckets[ 'sample' ],
			Key: key
		}).createReadStream().on('error', response => {
			next({ message: response.message, status: response.statusCode });
		}).pipe(res);
	} catch ( ex ) {
		next(ex);
	}
});

module.exports = router;
