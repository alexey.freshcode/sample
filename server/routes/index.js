const authCtrl = require('../controllers/auth');
const auth = require('../routes/auth');
const users = require('../routes/users');
const roles = require('../routes/roles');
const storage = require('../routes/storage');
const swaggerJSDoc = require('swagger-jsdoc');

const options = {
	definition: {
		info: {
			title: 'Sample',
			version: '1.0.0',
		},
	},
	apis: [
		require('path').resolve('./server/swagger.difinitions.yaml'),
		require('path').join(__dirname, '*.js'),
	], // Path to the API docs
};

// Initialize swagger-jsdoc -> returns validated swagger spec in json format
const swaggerSpec = swaggerJSDoc(options);

module.exports = (app) => {

	app.use('/api/auth', auth);

	app.use('/storage/', authCtrl.ensureAuthenticated, storage);

	app.use('/api/*', authCtrl.ensureAuthenticated);
	app.use('/api/users', users);
	app.use('/api/roles', roles);

	app.get('*', (req, res, next) => {
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader('Content-Type', 'application/json');
		res.send(swaggerSpec);
		// res.end('frontend should be here.');
	});

// error handler
// development error handler
// will print stacktrace
// production error handler
// no stacktraces leaked to user
	app.use((err, req, res, next) => {
		res.status(err.status || 500)
			.json({
				response: {
					type: 'error',
					message: err.message
				},
				error: app.get('env') === 'development' ? err : { status: err.status }
			});
	});

};
