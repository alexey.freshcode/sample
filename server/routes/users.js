const express = require('express');
const usersCtrl = require('../controllers/users');
const authCtrl = require('../controllers/auth');

const router = express.Router();

// CRUD
router.route('/')
	/**
	 * @swagger
	 * /api/users/:
	 *   get:
	 *     tags:
	 *      - users
	 *     summary: Find users
	 *     produces:
	 *       - application/json
	 *     parameters:
	 *       - in: query
	 *         required: true
	 *         name: limit
	 *         type: integer
	 *       - in: query
	 *         required: true
	 *         name: offset
	 *         type: integer
	 *     responses:
	 *       200:
	 *          type: object
	 *          properties:
	 *            totalCount:
	 *              type: integer
	 *            users:
	 *              type: array
	 *              items:
	 *                $ref: '#/definitions/FullUser'
	 */
	.get(usersCtrl.find)

	/**
	 * @swagger
	 * /api/users:
	 *   post:
	 *     tags:
	 *      - users
	 *     summary: Create user
	 *     produces:
	 *       - application/json
	 *     parameters:
	 *       - in: body
	 *         name: body
	 *         required: true
	 *         schema:
	 *          $ref: '#/definitions/User'
	 *     responses:
	 *       200:
	 *         description: User has been created
	 *         type: object
	 *         properties:
	 *          user:
	 *            $ref: '#/definitions/FullUser'
	 */
	.post(authCtrl.hasPermission('user-management', 'write'), usersCtrl.create);

router.route('/:userId')
	/**
	 * @swagger
	 * /api/users/{:userId}:
	 *   get:
	 *     tags:
	 *      - users
	 *     summary: Find user by ID
	 *     produces:
	 *       - application/json
	 *     parameters:
	 *       - in: path
	 *         required: true
	 *         name: userId
	 *         type: integer
	 *     responses:
	 *       200:
	 *         type: object
	 *         properties:
	 *          user:
	 *            $ref: '#/definitions/FullUser'
	 */
	.get(usersCtrl.findById)

	/**
	 * @swagger
	 * /api/users/{:userId}:
	 *   put:
	 *     tags:
	 *      - users
	 *     summary: Update user by ID
	 *     produces:
	 *       - application/json
	 *     parameters:
	 *       - in: path
	 *         required: true
	 *         name: userId
	 *         type: integer
	 *       - in: body
	 *         name: body
	 *         required: true
	 *         schema:
	 *          $ref: '#/definitions/User'
	 *     responses:
	 *       200:
	 *         description: User has been updated
	 *         type: object
	 *         properties:
	 *          user:
	 *            $ref: '#/definitions/FullUser'
	 */
	.put(authCtrl.hasPermission('user-management', 'write'), usersCtrl.update)

	/**
	 * @swagger
	 * /api/users/{:userId}:
	 *   delete:
	 *     tags:
	 *      - users
	 *     summary: Delete user by ID
	 *     parameters:
	 *       - in: path
	 *         required: true
	 *         name: userId
	 *         type: integer
	 *     responses:
	 *       200:
	 *         description: User has been deleted
	 */
	.delete(authCtrl.hasPermission('user-management', 'write'), usersCtrl.delete);

module.exports = router;
